// ------------------------------------------------------------------
// Transitive dependencies of this project determined from the
// maven pom organized by organization.
// ------------------------------------------------------------------

Browser Chat App


From: 'an unknown organization'
  - Guava: Google Core Libraries for Java (http://code.google.com/p/guava-libraries/guava) com.google.guava:guava:bundle:18.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - JavaBeans(TM) Activation Framework (http://java.sun.com/javase/technologies/desktop/javabeans/jaf/index.jsp) javax.activation:activation:jar:1.1.1
    License: COMMON DEVELOPMENT AND DISTRIBUTION LICENSE (CDDL) Version 1.0  (https://glassfish.dev.java.net/public/CDDLv1.0.html)
  - javax.inject (http://code.google.com/p/atinject/) javax.inject:javax.inject:jar:1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - servlet-api  javax.servlet:servlet-api:jar:2.5

  - jsr173_api  javax.xml.bind:jsr173_api:jar:1.0

  - "Java Concurrency in Practice" book annotations (http://jcip.net/) net.jcip:jcip-annotations:jar:1.0


From: 'Apache Software Foundation' (http://www.apache.org)
  - EJB 3.0 (http://geronimo.apache.org/specs/geronimo-ejb_3.0_spec) org.apache.geronimo.specs:geronimo-ejb_3.0_spec:jar:1.0.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - JTA 1.1 (http://geronimo.apache.org/specs/geronimo-jta_1.1_spec) org.apache.geronimo.specs:geronimo-jta_1.1_spec:jar:1.1.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)

From: 'FasterXML' (http://fasterxml.com)
  - Jackson (http://jackson.codehaus.org) org.codehaus.jackson:jackson-core-asl:jar:1.9.12
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - JAX-RS provider for JSON content type (http://jackson.codehaus.org) org.codehaus.jackson:jackson-jaxrs:jar:1.9.12
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)    License: GNU Lesser General Public License (LGPL), Version 2.1  (http://www.fsf.org/licensing/licenses/lgpl.txt)
  - Data Mapper for Jackson (http://jackson.codehaus.org) org.codehaus.jackson:jackson-mapper-asl:jar:1.9.12
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Xml Compatibility extensions for Jackson (http://jackson.codehaus.org) org.codehaus.jackson:jackson-xc:jar:1.9.12
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)    License: GNU Lesser General Public License (LGPL), Version 2.1  (http://www.fsf.org/licensing/licenses/lgpl.txt)

From: 'JBoss by Red Hat' (http://www.jboss.org)
  - JBoss Logging 3 (http://www.jboss.org) org.jboss.logging:jboss-logging:jar:3.3.0.Final
    License: Apache License, version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Resteasy Atom Provider (http://rest-easy.org/resteasy-atom-provider) org.jboss.resteasy:resteasy-atom-provider:jar:3.0.17.Final
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - Resteasy Jackson Provider (http://rest-easy.org/resteasy-jackson-provider) org.jboss.resteasy:resteasy-jackson-provider:jar:3.0.17.Final
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - Resteasy JAXB Provider (http://rest-easy.org/resteasy-jaxb-provider) org.jboss.resteasy:resteasy-jaxb-provider:jar:3.0.17.Final
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - RESTEasy JAX-RS Implementation (http://rest-easy.org/resteasy-jaxrs) org.jboss.resteasy:resteasy-jaxrs:jar:3.0.17.Final
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - Embedded Servlet Container (http://rest-easy.org/tjws) org.jboss.resteasy:tjws:jar:3.0.17.Final
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
  - Common Annotations 1.2 API (http://www.jboss.org/jboss-annotations-api_1.2_spec) org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec:jar:1.0.0.Final
    License: CDDL or GPLv2 with exceptions  (https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html)
  - JAX-RS 2.0: The Java(TM) API for RESTful Web Services (http://www.jboss.org/jboss-jaxrs-api_2.0_spec) org.jboss.spec.javax.ws.rs:jboss-jaxrs-api_2.0_spec:jar:1.0.0.Final
    License: Common Development and Distribution License  (http://repository.jboss.org/licenses/cddl.txt)    License: GNU General Public License, Version 2 with the Classpath Exception  (http://repository.jboss.org/licenses/gpl-2.0-ce.txt)

From: 'JBoss, a division of Red Hat' (http://www.jboss.org)
  - JGroups (http://www.jgroups.org) org.jgroups:jgroups:bundle:3.6.9.Final
    License: Apache License 2.0  (http://www.apache.org/licenses/LICENSE-2.0.html)

From: 'Oracle Corporation' (http://www.oracle.com/)
  - istack common utility code runtime (http://java.net/istack-commons/istack-commons-runtime/) com.sun.istack:istack-commons-runtime:jar:2.16
    License: CDDL 1.1  (https://glassfish.java.net/public/CDDL+GPL_1_1.html)    License: GPL2 w/ CPE  (https://glassfish.java.net/public/CDDL+GPL_1_1.html)
  - JAXB CORE (http://jaxb.java.net/) com.sun.xml.bind:jaxb-core:jar:2.2.7
    License: CDDL 1.1  (https://glassfish.java.net/public/CDDL+GPL_1_1.html)    License: GPL2 w/ CPE  (https://glassfish.java.net/public/CDDL+GPL_1_1.html)
  - JAXB Reference Implementation (http://jaxb.java.net/) com.sun.xml.bind:jaxb-impl:jar:2.2.7
    License: CDDL 1.1  (https://glassfish.java.net/public/CDDL+GPL_1_1.html)    License: GPL2 w/ CPE  (https://glassfish.java.net/public/CDDL+GPL_1_1.html)
  - Java Architecture for XML Binding (http://jaxb.java.net/) javax.xml.bind:jaxb-api:jar:2.2.7
    License: CDDL 1.1  (https://glassfish.java.net/public/CDDL+GPL_1_1.html)    License: GPL2 w/ CPE  (https://glassfish.java.net/public/CDDL+GPL_1_1.html)

From: 'Oracle Corpration' (http://www.oracle.com)
  - fastinfoset (http://fi.java.net) com.sun.xml.fastinfoset:FastInfoset:jar:1.2.12
    License: Apache License, Version 2.0  (http://www.opensource.org/licenses/apache2.0.php)

From: 'The Apache Software Foundation' (http://www.apache.org)
  - Annotation 1.1 (http://geronimo.apache.org/maven/specs/geronimo-annotation_1.1_spec/1.0.1) org.apache.geronimo.specs:geronimo-annotation_1.1_spec:bundle:1.0.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache Geronimo JMS Spec 2.0 (http://geronimo.apache.org/maven/specs/geronimo-jms_2.0_spec/1.0-alpha-2) org.apache.geronimo.specs:geronimo-jms_2.0_spec:bundle:1.0-alpha-2
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)

From: 'The Apache Software Foundation' (http://www.apache.org/)
  - Apache Commons BeanUtils (http://commons.apache.org/proper/commons-beanutils/) commons-beanutils:commons-beanutils:jar:1.9.2
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Commons Codec (http://commons.apache.org/codec/) commons-codec:commons-codec:jar:1.6
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache Commons Collections (http://commons.apache.org/collections/) commons-collections:commons-collections:jar:3.2.2
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Commons IO (http://commons.apache.org/io/) commons-io:commons-io:jar:2.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache Commons Logging (http://commons.apache.org/proper/commons-logging/) commons-logging:commons-logging:jar:1.2
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis Commons (http://apache.org/activemq/artemis-commons) org.apache.activemq:artemis-commons:jar:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis Core Client (http://apache.org/activemq/artemis-core-client) org.apache.activemq:artemis-core-client:jar:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis JDBC Store (http://apache.org/activemq/artemis-jdbc-store) org.apache.activemq:artemis-jdbc-store:jar:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis JMS Client (http://apache.org/activemq/artemis-jms-client) org.apache.activemq:artemis-jms-client:jar:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis JMS Server (http://apache.org/activemq/artemis-jms-server) org.apache.activemq:artemis-jms-server:jar:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis Journal (http://apache.org/activemq/artemis-journal) org.apache.activemq:artemis-journal:jar:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis Native POM (http://apache.org/activemq/artemis-native) org.apache.activemq:artemis-native:bundle:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis Selector Implementation (http://apache.org/activemq/artemis-selector) org.apache.activemq:artemis-selector:jar:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis Server (http://apache.org/activemq/artemis-server) org.apache.activemq:artemis-server:jar:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis Service Extensions (http://apache.org/activemq/artemis-service-extensions) org.apache.activemq:artemis-service-extensions:jar:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - ActiveMQ Artemis REST Interface Implementation (http://apache.org/activemq/artemis-rest) org.apache.activemq.rest:artemis-rest:jar:1.3.0
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache HttpClient (http://hc.apache.org/httpcomponents-client) org.apache.httpcomponents:httpclient:jar:4.3.6
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache HttpCore (http://hc.apache.org/httpcomponents-core-ga) org.apache.httpcomponents:httpcore:jar:4.3.3
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)

From: 'The Netty Project' (http://netty.io/)
  - Netty/All-in-One (http://netty.io/netty-all/) io.netty:netty-all:jar:4.0.32.Final
    License: Apache License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0)




